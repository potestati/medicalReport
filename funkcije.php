<?php
// --------FUNKCIJE----------

// konektovanje na bazu
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "dogadjaji";
// Create connection
$dbconnect = mysqli_connect($servername, $username, $password, $dbname);
// Check connection
if (!$dbconnect) {
	die("Connection failed: " . mysqli_connect_error());
}



Class Dogadjaj {
	
	// 1 - Kao administrator platforme trebalo bi da imam mogucnost da unesem novi dogadjaj ( naslov, opis, vreme odrzavanja, mesto odrzavanja ).
	public function unesi_dogadjaj() {
		// inicijalizujemo korištenje global promenjive unutar ove funkcije.
		global $dbconnect;
		global $naslov;
		global $opis;
		global $vreme;
		global $mesto;
		
		// unosimo sledece vrednosti kao novi red u tabeli dogadjaji
		$sql = "INSERT INTO dogadjaji (naslov, opis, vreme, mesto) VALUES ('$naslov', '$opis', '$vreme', '$mesto')";  
// unutra su jednostruki navodnici jer su spolja dvostruki.
		mysqli_query($dbconnect, $sql); // izvršavamo mYSQL query
	}
	
	// 2 - Kao administrator trebalo bi da budem u mogucnosti da izmenim postojeci dogadjaj.
	public function izmeni_dogadjaj($id) {
		// inicijalizujemo korištenje global promenjive unutar ove funkcije.
		global $dbconnect;
		global $naslov;
		global $opis;
		global $vreme;
		global $mesto;
		// menjamo red u bazi gde je ID koji smo primili kao argument funkcije
		$sql = "UPDATE dogadjaji SET naslov = '$naslov', opis = '$opis', vreme = '$vreme', mesto = '$mesto' WHERE id = '$id' LIMIT 1";
		mysqli_query($dbconnect, $sql);
	}
	
	// 3 - Kao administrator trebalo bi da imam mogucnost da obrisem dogadjaj.
	public function obrisi_dogadjaj($id) {
		// brisanje dogadjaja sa ID-om kji smo primili kao argument funkcije
		global $dbconnect; // inicijalizujemo korištenje global promenjive unutar ove funkcije.
		$sql = "DELETE FROM dogadjaji WHERE id = '$id' LIMIT 1";  // unutra su jednostruki navodnici jer su spolja dvostruki.
		mysqli_query($dbconnect, $sql); // izvršavamo MYSQL query
	}
	
	// 10 - Kao korisnik trebalo bi da mogu da prijavim svoj dolazak na dogadjaj. ---ATTEND NA FACEBOOKU
	public function prijava_za_dogadjaj($id) {
		// prijava za ucestvovanje na dogadjaj sa ID-om kji smo primili kao argument funkcije
		global $dbconnect; // inicijalizujemo korištenje global promenjive unutar ove funkcije.
		$sql = "INSERT INTO prijave (dogadjaj_id) VALUES ('$id')";  
// unutra su jednostruki navodnici jer su spolja dvostruki.
		mysqli_query($dbconnect, $sql); // izvršavamo mYSQL query
	}
}


