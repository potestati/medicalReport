<?php
// ZADATK PRACENJE DOGADJAJA

/*
Napraviti platformu za pracenje dogadjaja ( Events ). Na sledeci nacin:

GOTOVO 1 - Kao administrator platforme trebalo bi da imam mogucnost da unesem novi dogadjaj 
 * ( naslov, opis, vreme odrzavanja, mesto odrzavanja ).
GOTOVO 2 - Kao administrator trebalo bi da budem u mogucnosti da izmenim postojeci dogadjaj.
GOTOVO 3 - Kao administrator trebalo bi da imam mogucnost da obrisem dogadjaj.
4 - Kao administrator trebalo bi da imam pregled dogadjaja u listi sa paginacijom.
5 - Kao administrator trebalo bi da mogu da pretrazim postojece oglase.
6 - Kao administrator trebalo bi da imam pregled korisnika koji ce prisustvovati svakom specificnom dogadjaju.
GOTOVO 7 - Kao administrator trebalo bi da imam podatak na kojem dogadjaju ce prisustvovati najvise korisnika.

GOTOVO 8 - Kao korisnik platforme trebalo bi da imam pregled svih postavljenih dogadjaja.
9 - Kao korisnik platforme trebalo bi da mogu da filtriram dogadjaje po: vremenu odrzavanja, naslovu, po mestu odrzavanja.
GOTOVO 10 - Kao korisnik trebalo bi da mogu da prijavim svoj dolazak na dogadjaj. ---ATTEND NA FACEBOOKU
GOTOVO 11 - Kao korisnik ne bi trebalo da mogu da pristupim admin delu.

*/

// --------RESENJE: PROGRAMSKI KOD ZADATKA:



include 'funkcije.php'; // includujemo funkcije



// preuzimanje promenjivih iz URL-a

// po defaultu je role korisnik tj. anonimni posetilac, nije administrator.
$role = "korisnik"; 
// menjamo role korisnika ukoliko je primljeno kroz URL
if (isset($_GET["role"])) { // preuzimamo promenjive primljene iz url-a 
	$role = $_GET["role"];
}

$akcija="";
if (isset($_GET["akcija"])) { // preuzimamo promenjive primljene iz url-a 
	$akcija = $_GET["akcija"];
}

$id="";
if (isset($_GET["id"])) { // preuzimamo promenjive primljene iz url-a 
	$id = $_GET["id"];
}


// primanje novog dogadjaja iz forme
if ($role == "admin" && $akcija=="primi_novi") {
	// zadajemo default vrednosti da ne bi neki parametar bio preskočen.
	$naslov = "";
	$opis ="";
	$vreme = "";
	$mesto = "";
		
	// preuzimamo promenjive primljene iz url-a 
	if (isset($_GET["naslov"])) {
		$naslov = $_GET["naslov"];
	}
	if (isset($_GET["opis"])) {
		$opis = $_GET["opis"];
	}
	if (isset($_GET["vreme"])) {
		$vreme= $_GET["vreme"];
	}
	if (isset($_GET["mesto"])) {
		$mesto = $_GET["mesto"];
	}
	
	Dogadjaj::unesi_dogadjaj();
}

// primanje izmenjenog dogadjaja iz forme
if ($role == "admin" && $akcija=="primi_izmene") { // preuzimamo promenjive primljene iz url-a 
	// zadajemo default vrednosti da ne bi neki parametar bio preskočen.
	$id="";
	$naslov = "";
	$opis ="";
	$vreme = "";
	$mesto = "";
	
	// preuzimamo promenjive primljene iz url-a 
	if (isset($_GET["id"])) {
		$id = $_GET["id"];
	}
	if (isset($_GET["naslov"])) {
		$naslov = $_GET["naslov"];
	}
	if (isset($_GET["opis"])) {
		$opis = $_GET["opis"];
	}
	if (isset($_GET["vreme"])) {
		$vreme= $_GET["vreme"];
	}
	if (isset($_GET["mesto"])) {
		$mesto = $_GET["mesto"];
	}
	
	Dogadjaj::izmeni_dogadjaj($id);
}

// primanje komande za brisanje iz admin panela
if ($role == "admin" && $akcija=="obrisi") { // preuzimamo promenjive primljene iz url-a 
	// ako je korisnik administrator moze da obrise dogadjaj
	
	// zadajemo default vrednosti da ne bi neki parametar bio preskočen.
	$id="";
	// preuzimamo promenjive primljene iz url-a 
	if (isset($_GET["id"])) {
		$id = $_GET["id"];
	}
	
	Dogadjaj::obrisi_dogadjaj($id); // obavi brisanje
}

// primanje komande za prijavu za učestvovanje na dogadjaju
if ($role !== "admin" && $akcija=="prijavi") {
	// ako korisnik niej administrator moze da se prijavi na dogadjaj
	
	// zadajemo default vrednosti da ne bi neki parametar bio preskočen.
	$id="";
	// preuzimamo promenjive primljene iz url-a 
	if (isset($_GET["id"])) {
		$id = $_GET["id"];
	}
	
	Dogadjaj::prijava_za_dogadjaj($id); // obavi prijavu
}



include 'template.php'; // includujemo template

// KRAJ RESENJA ZADATKA
