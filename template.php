<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf8" />
        <title>ZADATAK DOGADJAJI</title>
        <!-- TEMPLATE ZA ZADATAK DOGADJAJI -->
    </head>
    <body>
        <?php
        if ($role == "admin") {
            // ako je korisnik administrator
            // prikazi administratorske linkove na vrhu strane
            ?>
            Administratorske opcije: <br>
            <a href ="index.php?role=admin&akcija=pregled">Pregled svih dodgadjaja</a><br>
            <a href ="index.php?role=admin&akcija=unesi">Unesi novi dogadjaj</a><br>
            <br>
            <br>
            <?php
        } else {
            // ako korisnik nije administrator
            // prikazi samo link za prikaz dogadjaja
            ?>
            <a href ="index.php?akcija=pregled">Pregled svih dodgadjaja</a><br>
            <br>
            <br>
            <?php
        }

        if ($role == "admin" && $akcija == "unesi") {
            // ako je administrator i ako je izabrano unos novog dodgadjaja
            // prikazi web formu za unos novog dogadjaja
            ?>
            <form action="index.php" method="get">
                <p>
                <h1>unos novog dogadjaja</h1>
            </p>
            <p>
                Naslov: <input type="text" name="naslov">
            </p>
            <p>
                Opis: <input type="text" name="opis">
            </p>
            <p>
                Vreme: <input type="text" name="vreme">
            </p>
            <p>
                Mesto: <input type="text" name="mesto">
            </p>
            <input type="hidden" name="role" value="admin"><!-- takodje saljemo promenjivu da je role admin -->
            <input type="hidden" name="akcija" value="primi_novi"><!-- takodje saljemo komandu za odgovarajuci prijem izmena -->
            <input type="submit" value="Submit">
        </form>
        <?php
    }


    if ($role == "admin" && $akcija == "izmeni") {
        // ako je administrator i ako je izabrano izmena dogadjaja
        $row = mysqli_fetch_assoc(mysqli_query($dbconnect, "SELECT * FROM dogadjaji WHERE id = '$id'"));
        //// pravimo array iz tabele dogadjaji sa samo jednim redom gde je trazeni ID
        // prikazi web formu za izmenu
        ?>
        <form action="index.php?role=admin&akcija=primi_izmene" method="get">
            <p>
            <h1>Izmeni dogadjaj</h1>
        </p>
        <p>
            Naslov: <input type="text" name="naslov" value="<?php echo $row['naslov']; ?>">
        </p>
        <p>
            Opis: <input type="text" name="opis" value="<?php echo $row['opis']; ?>">
        </p>
        <p>
            Vreme: <input type="text" name="vreme" value="<?php echo $row['vreme']; ?>">
        </p>
        <p>
            Mesto: <input type="text" name="mesto" value="<?php echo $row['mesto']; ?>">
        </p>
        <input type="hidden" name="id" value="<?php echo $row['id']; ?>"><!-- takodje saljemo i ID koji smo upravo izmenili -->
        <input type="hidden" name="role" value="admin"><!-- takodje saljemo promenjivu da je role admin -->
        <input type="hidden" name="akcija" value="primi_izmene"><!-- takodje saljemo komandu za odgovarajuci prijem izmena -->
        <input type="submit" value="Submit">
    </form>
    <?php
}

if ($akcija == "pregled" || $akcija == "") {
    // ako je izabrano ispis liste dogadjaja
    // prikazi listu svih dogadjaja
    ?>
    <h1>Pregled svih dogadjaja</h1>
    <?php
    // pravimo array sa svim podacima iz tabele dogadjaji
    $sql = "SELECT * FROM dogadjaji ORDER BY vreme ASC"; // uzima sve dogadjaje i sortira po vremenu od 0-9 i abecedno od A do Z
    $res = mysqli_query($dbconnect, $sql);
    while ($row = mysqli_fetch_assoc($res)) {
        $id = $row['id'];
        $broj_prijava = mysqli_num_rows(mysqli_query($dbconnect, "SELECT id FROM prijave WHERE dogadjaj_id = '$id'")); // prebrojava broj prijavljenih na ovaj dogadjaj
        ?>
        <div class="dogadjaj">
            <p>
            <h2>
                <?php echo $row['naslov']; ?>
            </h2>
        </p>
        <p>
            <?php echo "Opis: " . $row['opis']; ?>
        </p>
        <p>
            <?php echo "Vreme odrzavanja: " . $row['vreme']; ?>
        </p>
        <p>
            <?php echo "Mesto odrzavanja: " . $row['mesto']; ?>
        </p>

        <?php
        if ($role == "admin") {
            // ako je administrator
            // prikazi broj prijavljenih i dodatne linkove za izmenu i brisanje
            ?>
            <p>
                <?php echo "Broj prijavljenih: " . $broj_prijava; ?>
            </p>
            <a href="index.php?role=admin&akcija=izmeni&id=<?php echo $row['id'] ?>">izmeni</a>&nbsp;&nbsp;&nbsp;<a href="index.php?role=admin&akcija=obrisi&id=<?php echo $row['id'] ?>">obrisi</a>
            <?php
        }
        if ($role == "korisnik") {
            // ako je obican korisnik
            // prikazi dodatne linkove za izmenu i brisanje
            ?>
            <a href="index.php?role=korisnik&akcija=prijavi&id=<?php echo $row['id'] ?>">Prijavi se za učestvovanje</a>
            <?php
        }
        ?>
        <br>
        <br>
        </div>
        <?php
    }
}
?>

</body>
</html>